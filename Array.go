package main

import "fmt"

func main() {

	fmt.Print("Введите длину массива: ")
	var N int
	fmt.Scan(&N)

	fmt.Println("Введите числа массива:")
	var a []float64

	for i:=0;i<N;i++ {
		var x float64
		fmt.Scan(&x)
		a = append(a,x)
	}
	fmt.Println("Массив:", a)

	for i:=0;i<N;i++ {
		a[i] = a[i] * 1.1
	}
	fmt.Println("Массив умноженный на 10%: ", a)

	var s = false
	for !s {
		s = true
		for i:=0;i<N-1;i++ {
			if a[i] > a [i+1] {
				s = false
				var b = a[i]
				a[i] = a[i+1]
				a[i+1] = b
			}
		}
	}
	fmt.Println("Сортировка пузырьком: ", a)
}
